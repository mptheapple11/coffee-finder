//
//  GooglePlaces.h
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for GooglePlaces.
FOUNDATION_EXPORT double GooglePlacesVersionNumber;

//! Project version string for GooglePlaces.
FOUNDATION_EXPORT const unsigned char GooglePlacesVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GooglePlaces/PublicHeader.h>


