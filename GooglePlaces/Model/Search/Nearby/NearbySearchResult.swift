//
//  NearbySearchResult.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

public extension NearbySearch {
    struct Response: Model {
        public let results: [Result]
    }
    
    /// Result of a nearby search
    struct Result: Model {
        public let placeID: String
        public let geometry: Geometry
        public let name: String
        public let icon: String
        
        enum CodingKeys: String, CodingKey {
            case placeID = "place_id"
            case geometry
            case name
            case icon
        }
    }
}

public struct Geometry: Model {
    public let location: Location
}
