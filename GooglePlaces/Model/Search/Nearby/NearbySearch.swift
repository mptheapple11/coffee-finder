//
//  NearbySearch.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Foundation

/// Namespace for nearby search
public struct NearbySearch { }

public protocol Coordinate {
    typealias Degrees = Double
    var latitude: Degrees { get }
    var longitude: Degrees { get }
}

// Location parameter for a nearby search
public struct Location: Model, Coordinate {
    public let latitude: Degrees
    public let longitude: Degrees
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
    
    public init(_ coordinate: Coordinate) {
        self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
    
    public init(latitude: Degrees, longitude: Degrees) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    // custom encoder to generate the formatted string that google expects "lat,long"
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode("\(latitude),\(longitude)")
    }
}

