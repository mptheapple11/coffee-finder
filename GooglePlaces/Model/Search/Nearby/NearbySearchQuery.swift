//
//  NearbySearchQuery.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

public extension NearbySearch {
    /// Query parameter values for a nearby search
    struct Query: Model {
        /// The search term to search for
        public let text: String
        /// The location to search nearby
        public let location: Location
        /// The search radius from the location to search in meters
        public let radius: Int
        
        public init(text: String, coordinate: Coordinate, radius: Int = 1000) {
            self.text = text
            self.radius = radius
            self.location = Location(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
        
        enum CodingKeys: String, CodingKey {
            case text = "keyword"
            case location
            case radius
        }
    }
}
