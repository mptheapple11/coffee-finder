//
//  PlaceDetailResult.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Foundation

public extension PlaceDetail {
    struct Response: Model {
        public let result: Result
    }
    
    /// The result of a Google Place detail call
    struct Result: Model {
        public let placeID: String
        public let name: String?
        public let address: String?
        public let hours: Hours?
        public let photos: [Photo]
        
        enum CodingKeys: String, CodingKey {
            case placeID = "place_id"
            case address = "formatted_address"
            case hours = "opening_hours"
            case name
            case photos
        }
    }
}
