//
//  PlaceDetailQuery.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Foundation

public typealias PlaceID = String

public extension PlaceDetail {
    /// Query for place detail
    struct Query: Model {
        public let placeID: PlaceID
        
        public init(placeID: PlaceID) {
            self.placeID = placeID
        }
        
        enum CodingKeys: String, CodingKey {
            case placeID = "place_id"
        }
    }
    
}

