//
//  Photo.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/26/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Foundation

public struct Photo: Model {
    public let id: String
    
    enum CodingKeys: String, CodingKey {
        case id = "photo_reference"
    }
}
