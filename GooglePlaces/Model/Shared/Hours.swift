//
//  Hours.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/26/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Foundation

public struct Hours: Model {
    public let openNow: Bool
    
    enum CodingKeys: String, CodingKey {
        case openNow = "open_now"
    }
}
