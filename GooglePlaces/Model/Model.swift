//
//  Model.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import FoundationExt

public typealias Model = Codable & JSONEncodable
