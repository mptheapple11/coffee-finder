//
//  Client.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Services

public struct GooglePlacesClient {
    /// The network client that manages and queues requests
    private let client: Client
    /// The environment that the client is pointed at
    public let environment: Environment
    
    /// The endpoint that handles searching the Google Places API
    public let search: SearchEndpoint
    /// The endpoint that handles getting the details for a Google Place
    public let detail: DetailEndpoint
    /// The endpoint that handles photos associated with a Google Place
    public let photo: PhotoEndpoint
    
    public init(environment: Environment = .dev) {
        // instantiate client with the parameterized env
        client = Client(environment: environment)
        self.environment = environment
        
        // configure each endpoint with the client
        search = SearchEndpoint(client: client)
        detail = DetailEndpoint(client: client)
        photo = PhotoEndpoint(client: client)
    }
}
