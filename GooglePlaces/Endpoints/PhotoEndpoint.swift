//
//  PhotoEndpoint.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Services


public struct PhotoQuery: Model {
    let id: String
    let maxHeight: Float
    let maxWidth: Float
    
    enum CodingKeys: String, CodingKey {
        case id = "photo_reference"
        case maxHeight = "maxheight"
        case maxWidth  = "maxwidth"
    }
    
    public init(photo: Photo, maxHeight: Float, maxWidth: Float) {
        self.id = photo.id
        self.maxWidth = maxWidth
        self.maxHeight = maxHeight
    }
}

public struct PhotoEndpoint {
    let client: Client
    
    public func url(for query: PhotoQuery) -> URL? {
        let request = Request(method: .get, path: "/photo", parameters: query)
        return request.urlRequest(for: client.environment).url
    }
}
