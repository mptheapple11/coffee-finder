//
//  SearchEndpoint.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import FoundationExt
import Services

public struct SearchEndpoint {
    let client: Client
    
    /// Retrieves Google Places matching the paramterized query
    public func nearbyPlaces(for query: NearbySearch.Query, completion: @escaping (NearbySearch.Response?, Error?) -> Void) {
        client.model(request: Request(method: .get, path: "/nearbysearch/json", parameters: query), completion)
    }
}

