//
//  DetailEndpoint.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Services

public struct DetailEndpoint {
    let client: Client
    
    /// Retrieves the detail metadata for a Google Place
    public func detail(for query: PlaceDetail.Query, completion: @escaping (PlaceDetail.Response?, Error?) -> Void) {
        client.model(request: Request(method: .get, path: "/details/json", parameters: query), completion)
    }
}

