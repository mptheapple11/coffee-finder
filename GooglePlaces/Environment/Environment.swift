//
//  Environment.swift
//  GooglePlaces
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Services

extension Environment {
    /// Google Places development environment
    public static let dev = Environment(
        hostname: "maps.googleapis.com",
        basePath: "maps/api/place",
        defaultParameters: [
            "key": "AIzaSyBppvfxox3vTmVhUaXtuu0_Gwa4bq6Vd7s"
        ]
    )
}
