Fooda Programming Challenge

* Project was built against Xcode 11.4.1
* Project does not contain any external dependencies and should build and run without any setup
* The Places API key did not work for me, said that it had either been revoked or it did not exist so I used my own
* I could not use the link to submit my repo, this was the error message that came up “We were not able to add the user to the Assignment, please try again. (Repository: You must purchase at least one more seat to add this user as a collaborator.)”


High Level Overview:

I chose to implement the challenge using Swift, and a very lean MVC architecture. Each ViewController leverages coordinator objects and extensions on UIKit and Foundation elements to offload the majority of what you would normally find in a “Massive-View-Controller” implementation.

The service layer is a very thin wrapper around URLSession allowing for easy request configuration, and model encoding/decoding using Codable conformance. This core service layer is meant to be instantiated as many times as there are unique backends the app is interested in. The app target has a Services struct which serves as a singular source for service client singletons. GooglePlaces is the only one there currently, but if this were a true Fooda application we would have at least one maybe a couple different Fooda service clients as well.

I created extension targets for each of the iOS SDK frameworks I used to serve as repositories for Fooda enhancements, allowing other applications to pull in them in ala carte without having to take the entire stack.

Test Plan:

The iOS platform is painfully underdeveloped when it comes to testing when compared with tools found for web development. If I were to build out a comprehensive test suite we need to solve for a few things:

Unit Tests:

1. We need some way to mock the service layer without introducing test specific logic into our production code. I have used OHTTPStubs for this in the past. Mocks can be stored in json files and loaded from disk strongly typed with a simple static extension on Model

	extension Model {
		static func mock(from filepath: String) -> Self { }
	}

2. We need to strike a careful balance between protocol based dependency injection and exposing too much of an objects internal workings as public API. Each dependency that we inject requires more knowledge of what that object is going to do at each call site it is instantiated

3. Instantiate Coordinator, ViewController, View and act on those objects by setting state and or calling functions, assert that the object reflects the state we expect
    1. In a MVVM/Viper world we would be able to keep our Unit tests isolated to Foundation instead of having to pull in UIKit to test ViewControllers, Views, etc

Integration Tests:

1. Xcode spawns two processes when integration tests run, the first is the process where the app is run and the 2nd is the process where your tests run. This makes it a true black box test, where the developer can only interact with the app as if they were a user interacting with the app through gestures on the screen. The only other way we can influence the behavior of the app is through launch arguments.

2. We still need a way to mock services but this is more difficult since we don’t have access to the app process, in the past I have spun up a simple node express server that served up canned responses which made testing as easy as sending a launch argument to tell the app to use the test environment (i.e. localhost). I like this approach because we are not meddling with the production logic at all, just having our service layer point at a different environment. Drawback is that the team will need to maintain the express server and its endpoints/mocks though this approach can be used for unit tests as well and get rid of the need for OHHTPStubs or the like

3. In order to scale integration tests, I would create abstract objects to model Screens and their Components. When you use the Xcode Integration Test recorder, it spits out a lot of code that performs gestures and queries for UI by ID. This logic is often repeated 10x as we perform flows many times with small variations to test each use case. Screens and Components are a way of defining elements with their IDs a single time and defining actions that can be taken on a screen so that each integration test is just a composition of those things

Simple and arbitrary example:

    enum MapScreenComponents: String {
        case starbucksAnnotation = "starbucks_annotation"
        case locateUserButton = "locate_user_button"
    }

    class Screen<Components> {
        init(app: XCUIApplication) { }

        func tap(_ component: Component) { }

        func find(_ component: Component) -> XCUIElement { }
    }

    class MapScreen: Screen<MapScreenComponents> {
        func panMap(to point: CLLocationCoordinate2D) { }
        func selectStarbucksAnnotation() { }
    }

4. Integration tests need to start from a known state and one of the easiest ways to achieve this is to restart the application, in this case our Screen/Component abstraction will come in handy because interacting with the app becomes a process of instantiating Screens and using their functions to interact with them and navigate to other screens. If a screen changes or an interaction changes, updating the screen API will make it very clear what other tests are going to break at compile time instead of run time.

5. As a side note, I have used KIF (https://github.com/kif-framework/KIF) in the past with great success at a previous job. I modeled the solution after this awesome post from Peter Steinberger from PSPDFKit https://pspdfkit.com/blog/2016/running-ui-tests-with-ludicrous-speed/ The awesome thing about KIF is that it lets your unit tests drive the integration test which gives you access to the app process. That combined with Steinbergers speed enhancements made a huge difference in our test suite though this was a few years back so there might be better solutions at this point.

