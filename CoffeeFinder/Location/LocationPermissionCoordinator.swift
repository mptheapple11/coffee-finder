//
//  LocationPermissionRequest.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit
import UIKitExt
import CoreLocation

public struct LocationPermissionCoordinator {
    let locationManager = CLLocationManager()
    
    func promptForAuthorizationStatus() -> AlertBuilder? {
        let status = CLLocationManager.authorizationStatus()
        // bail out if we are already authorized
        if status.isAuthorized { return nil }
        
        // prompt for permission
        let authorizationUndetermined = status == .notDetermined
        return AlertBuilder(title: "Coffee Finder needs access to your location", message: "This will allow us to show you coffee shops near you!", actions: [
            .ok(included: authorizationUndetermined) { _ in
                self.locationManager.requestWhenInUseAuthorization()
            },
            .default(included: !authorizationUndetermined, title: "Go To Settings") { _
                in UIApplication.shared.open(UIApplication.URLs.openSettings)
            },
            .cancel(title: "Not Now")
        ])
    }
}
