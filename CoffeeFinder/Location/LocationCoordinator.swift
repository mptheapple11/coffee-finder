//
//  LocationCoordinator.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit
import CoreLocation

class LocationCoordinator: NSObject {
    private let locationManager = CLLocationManager()
    private let onChangeAuthorization: OnChangeAuthorization?
    private let onUpdateLocations: OnUpdateLocations?
    
    /// Closure based implementation of the CLLocationManagerDelegate method of the same name
    typealias OnChangeAuthorization = (CLAuthorizationStatus) -> Void
    /// Closure based implementation of the CLLocationManagerDelegate method of the same name
    typealias OnUpdateLocations = ([CLLocation]) -> Void
    
    private var authorizationStatus: CLAuthorizationStatus = CLLocationManager.authorizationStatus()
    
    init(onChangeAuthorization: OnChangeAuthorization? = nil, onUpdateLocations: OnUpdateLocations? = nil) {
        self.onChangeAuthorization = onChangeAuthorization
        self.onUpdateLocations = onUpdateLocations
        
        super.init()
        
        // we are our own delegate
        locationManager.delegate = self
        locationManager.distanceFilter = 100
    }
    
    func bootstrap() {
        // start updating location if we are already authorized, otherwise we will start when auth status changes
        if authorizationStatus.isAuthorized {
            locationManager.startUpdatingLocation()
        }
    }
}


extension LocationCoordinator: CLLocationManagerDelegate {
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        authorizationStatus = status
        
        switch status {
        case .notDetermined:
            print("Location status not determined.")
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
        case .authorizedWhenInUse:
            print("Location status is authorized when in use.")
            locationManager.startUpdatingLocation()
        case .authorizedAlways:
            print("Location status is authorized always")
            locationManager.startUpdatingLocation()
        @unknown default:
            print("Location status is unknown.")
        }
        
        // call our handler if we have one
        onChangeAuthorization?(status)
    }
    
    // handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // call our handler if we have one
        onUpdateLocations?(locations)
    }
    
    // handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}

extension CLAuthorizationStatus {
    /// True if authorizationStatus is always or whenInUse
    var isAuthorized: Bool {
        self == .authorizedAlways || self == .authorizedWhenInUse
    }
}
