//
//  Services.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import GooglePlaces

public struct Services {
    static let googlePlaces = GooglePlacesClient()
}
