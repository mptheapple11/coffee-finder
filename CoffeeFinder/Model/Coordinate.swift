//
//  Coordinate.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import MapKit
import GooglePlaces

// Makes it easier to convert in and out of Google/MapKit coordinates

extension CLLocationCoordinate2D: Coordinate {
    init(_ coordinate: Coordinate) {
        self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
}

extension CLLocation: Coordinate {
    public var latitude: Degrees { coordinate.latitude }
    public var longitude: Degrees { coordinate.longitude }
    
    public convenience init(_ coordinate: Coordinate) {
        self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
}
