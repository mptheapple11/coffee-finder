//
//  ViewController.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GooglePlaces

extension MapViewController {
    struct Defaults {
        // Default to Chicago if we don't have access to user location
        static let location = CLLocation(latitude: 41.884471, longitude: -87.634781)
        // Search radius from either the default location or user location
        static let searchRadius = 1000
        // Search term
        static let searchTerm = "Coffee Shop"
    }
}

class MapViewController: UIViewController {
    @IBOutlet private weak var mapView: MKMapView!
    
    private let permission = LocationPermissionRequest()
    
    private lazy var coordinator = LocationCoordinator(
        onChangeAuthorization: { [unowned self] status in
            if !CLLocationManager.isAuthorized() {
                self.performSearch(at: Defaults.location)
            }
        },
        onUpdateLocations: { [unowned self] locations in
            guard let location = locations.first else { return }
            self.performSearch(at: location)
        }
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        mapView.showsUserLocation = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        permission.resolveAuthorizationStatus(from: self)
    }
    
    private func performSearch(at location: CLLocation) {
        mapView.centerToLocation(location)
        
        Services.googlePlaces.search
            .nearbyPlaces(for: .init(text: Defaults.searchTerm, coordinate: location, radius: Defaults.searchRadius)) { response, error in
                let annotations = response?.results.map(CoffeeShopAnnotation.init) ?? []
                self.mapView.showAnnotations(annotations, animated: true)
            }
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? CoffeeShopAnnotation else { return nil }
        
        return mapView.dequeueReusableAnnotationView(for: annotation) { annotation, identifier in
            let view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.displayPriority = .required
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            return view
        }
    }
}
