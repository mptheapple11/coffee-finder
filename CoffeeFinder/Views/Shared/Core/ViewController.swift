//
//  ViewController.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/26/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit

/// Clearly defines the ViewController's dependencies via the generic TransitionModel
class ViewController<TransitionModel>: UIViewController {
    var transitionModel: TransitionModel? = nil
}
