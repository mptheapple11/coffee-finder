//
//  UIViewController+Navigation.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/26/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit

extension UINavigationController {
    /// Pushes the parameterized viewControllerType
    func push<T>(_ viewControllerType: ViewController<T>.Type, transitionModel: T? = nil, storyboard: Bool = true, animated: Bool = true) {
        let destination = storyboard ? viewControllerType.instance() : viewControllerType.init()
        destination.transitionModel = transitionModel
        
        pushViewController(destination, animated: animated)
    }
}
