//
//  NavigationViewController.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController, UIGestureRecognizerDelegate {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        navigationBar.tintColor = .white
        navigationBar.barTintColor = .systemRed
        navigationBar.shadowImage = UIImage()
        
        navigationBar.titleTextAttributes = [
            .foregroundColor: UIColor.white
        ]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

