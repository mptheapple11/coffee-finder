//
//  CoffeeShopAnnotation.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Foundation
import MapKit
import MapKitExt
import GooglePlaces

class CoffeeShopAnnotation: Annotation<NearbySearch.Result>, MKAnnotation {
    var title: String? { model.name }

    var subtitle: String? { model.name }
    
    var image: UIImage { #imageLiteral(resourceName: "coffee-cup-pin") }
    
    var coordinate: CLLocationCoordinate2D { .init(model.geometry.location) }
}
