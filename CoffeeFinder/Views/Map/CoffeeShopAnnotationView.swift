//
//  CoffeeShopAnnotationView.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit
import MapKitExt

class CoffeeShopAnnotationView: AnnotationView<CoffeeShopAnnotation> {
    override var model: CoffeeShopAnnotation? {
        didSet {
            displayPriority = .required
            glyphImage = model?.image
        }
    }
}
