//
//  ViewController.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit
import MapKit

extension MapViewController {
    struct Defaults {
        // Default to Chicago if we don't have access to user location
        static let location = CLLocation(latitude: 41.884471, longitude: -87.634781)
        // Search radius from either the default location or user location
        static let searchRadius = 1000
        // Search term
        static let searchTerm = "Coffee Shop"
    }
}

class MapViewController: UIViewController {
    @IBOutlet private weak var mapView: MKMapView!
    private lazy var trackingBarButton = MKUserTrackingBarButtonItem(mapView: mapView)
    
    /// Manages requesting user permission for location services
    private let locationPermissionCoordinator = LocationPermissionCoordinator()
    /// Manages user location authorization changes, and user location updates
    private lazy var locationCoordinator = LocationCoordinator(
        onChangeAuthorization: { [unowned self] status in
            // show or hide the tracking button based on authorization
            self.navigationItem.setRightBarButton(status.isAuthorized ? self.trackingBarButton : nil, animated: true)
            
            if !status.isAuthorized  {
                // search at the default location if we don't have user location permissions
                self.performSearch(at: Defaults.location)
            }
        },
        onUpdateLocations: { [unowned self] locations in
            guard let location = locations.first else { return }
            // re-run the search at the user locations whenever it updates
            self.performSearch(at: location)
        }
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // start listening for location updates and authorization changes
        locationCoordinator.bootstrap()
        // prompt user for location authorization if we don't have it
        if let prompt = locationPermissionCoordinator.promptForAuthorizationStatus() {
            prompt.present(from: self)
        }
    }
    
    private func performSearch(at location: CLLocation) {
        // center the map at the new location
        mapView.centerOn(location)
        
        // request the coffee shops at the new location
        Services.googlePlaces.search
            .nearbyPlaces(for: .init(text: Defaults.searchTerm, coordinate: location, radius: Defaults.searchRadius)) { response, error in
                // map the coffee shops we get back to annotations and display them
                let annotations = response?.results.map(CoffeeShopAnnotation.init) ?? []
                self.mapView.removeAnnotations(self.mapView.annotations)
                self.mapView.showAnnotations(annotations, animated: true)
            }
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? CoffeeShopAnnotation else { return nil }
        
        // unified dequeue / init logic
        return mapView.dequeueReusableAnnotationView(ofType: CoffeeShopAnnotationView.self, for: annotation) { view, annotation in
            view.model = annotation
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        // would be useful in the future to wrap these delegate methods to be strongly typed using a data driven API instead of all this type inspection at each of the delegate method implementations
        guard let placeID = (view.annotation as? CoffeeShopAnnotation)?.model.placeID else { return }
        
        navigationController?.push(DetailViewController.self, transitionModel: DetailTransitionModel(placeID: placeID))
    }
}
