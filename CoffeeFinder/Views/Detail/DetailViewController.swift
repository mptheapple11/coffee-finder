//
//  DetailViewController.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit
import UIKitExt
import GooglePlaces

struct DetailTransitionModel {
    let placeID: PlaceID
}

class DetailViewController: ViewController<DetailTransitionModel> {
    @IBOutlet private weak var heroImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var openNowLabel: UILabel!
    
    private var model: PlaceDetail.Result? = nil {
        didSet {
            titleLabel.text = model?.name
            addressLabel.text = model?.address
            openNowLabel.text = model?.openStatusDescription
            
            if let photo = model?.photos.first {
                let photoURL = Services.googlePlaces.photo
                    .url(for: .init(photo: photo, maxHeight: 300, maxWidth: 300))
                heroImageView.load(photoURL)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let placeID = transitionModel?.placeID {
            performDetailRequest(for: placeID)
        }
    }
    
    private func performDetailRequest(for placeID: PlaceID) {
        Services.googlePlaces.detail
            .detail(for: .init(placeID: placeID)) { response, error in
                self.model = response?.result
            }
    }
}

fileprivate extension PlaceDetail.Result {
    var openStatusDescription: String {
        guard let hours = hours else { return "Hours Unknown" }
        return hours.openNow ? "Open Now" : "Closed"
    }
}
