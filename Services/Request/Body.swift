//
//  Body.swift
//  Services
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import FoundationExt

/// Describes an object that can be encoded as the body of a request
public protocol Body {
    /// Hook to retrieve body data
    var data: Data? { get }
    /// Allows an environment to be applied to the data, most significantly the default body fields
    mutating func apply(_ environment: Environment)
}

/// Body that accepts [String: Any] or Encodable instances
public struct JSONBody: Body {
    var json: JSONEncodable?

    init(json: JSONEncodable?) {
        self.json = json
    }
    
    /// Appends the parameterized parameters to the request
    mutating public func appendParameters(_ append: JSON) {
        json = combineParameters(lhs: json?.encoded(), rhs: append)
    }
    
    /// Combines lhs and rhs into a single parameter dictionary, key conflicts are resolved using rhs
    private func combineParameters(lhs: Parameters?, rhs: Parameters?) -> Parameters? {
        // if we don't have lhs, return rhs
        guard let lhs = lhs else {
            return rhs
        }
        
        // if we don't have rhs, return lhs
        guard let rhs = rhs else {
            return lhs
        }
        
        // othwerwise if we have both, combine them resolving key conflicts by taking rhs
        return lhs.merging(rhs, uniquingKeysWith: { (_, new) -> Any in
            return new
        })
    }
    
    public mutating func apply(_ environment: Environment) {
        json = combineParameters(lhs: environment.defaultParameters, rhs: json?.encoded())
    }
    
    public var data: Data? {
        guard let object = json?.encoded() else { return nil }
        return try? JSONSerialization.data(withJSONObject: object, options: .fragmentsAllowed)
    }
}
