//
//  URLRequest+Convenience.swift
//  Services
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import FoundationExt

public extension URLRequest {
    init(method: HTTPMethod, url: URL, body: Body? = nil, headers: Headers? = nil, queryParameters: QueryParameters? = nil) {
        self.init(method: method, path: url.absoluteString, body: body, headers: headers, queryParameters: queryParameters)
    }
    
    init(method: HTTPMethod, path: String, body: Body? = nil, headers: Headers? = nil, queryParameters: QueryParameters? = nil) {
        var path = path
        var queryParameters: JSON = queryParameters ?? [:]
        
        // if we are a get request and we have parameters, add them to the path
        if method == .get, let data = body?.data, let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? JSON {
            queryParameters = queryParameters.merging(json, uniquingKeysWith: { lhs, rhs -> Any in rhs })
        }
        
        if queryParameters.count > 0 {
            path = path + "?\(queryParameters.queryString())"
        }
        
        // generate the absolute url from the path and base
        let url = URL(string: path)!
        // instantiate the request
        var request: URLRequest = URLRequest(url: url)
        // set the http method
        request.httpMethod = method.rawValue
        
        // if we are a put or a post and we have parameters encode them in the body
        if method == .put || method == .post, let body = body {
            request.httpBody = body.data
        }
        
        // apply the headers to the request
        headers?.forEach({ (pair) in
            request.setValue(pair.value, forHTTPHeaderField: pair.key)
        })
        
        self = request
    }
}
