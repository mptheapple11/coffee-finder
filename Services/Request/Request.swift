//
//  Request.swift
//  Services
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import FoundationExt

public typealias Parameters = JSON
public typealias Headers = [String: String]
public typealias QueryParameters = [String: String]

public class Request {
    /// The path of the network resource the request is for
    let path: String
    /// The HTTPMethod of the request (get, post, put, etc)
    let method: HTTPMethod
    /// Parameters will be encoded on the url for get, and encoded in the body for post
    var body: Body?
    /// Query params to be encoded into the path
    var queryParameters: QueryParameters?
    /// Headers for the request
    var headers: Headers?
    /// True if the request needs to be authenticated
    let authenticated: Bool
    /// True if the request should be made against the client's environment
    let applyEnvironment: Bool
    
    public convenience init(method: HTTPMethod, url: URL, authenticated: Bool = true,
        applyEnvironment: Bool = true, parameters: JSONEncodable? = nil, queryParameters: QueryParameters? = nil, headers: Headers? = nil) {
        self.init(method: method, path: url.absoluteString, authenticated: authenticated, applyEnvironment: applyEnvironment, parameters: parameters, queryParameters: queryParameters, headers: headers)
    }
    
    public convenience init(method: HTTPMethod, path: String, authenticated: Bool = true,
        applyEnvironment: Bool = true, parameters: JSONEncodable? = nil, queryParameters: QueryParameters? = nil, headers: Headers? = nil) {
        self.init(method: method, path: path, authenticated: authenticated, applyEnvironment: applyEnvironment, body: JSONBody(json: parameters), queryParameters: queryParameters, headers: headers)
    }
    
    private init(method: HTTPMethod, path: String, authenticated: Bool = true,
        applyEnvironment: Bool = true, body: Body? = nil, queryParameters: QueryParameters? = nil, headers: Headers? = nil) {
        self.path = path
        self.method = method
        self.body = body
        self.headers = headers
        self.authenticated = authenticated
        self.applyEnvironment = applyEnvironment
        self.queryParameters = queryParameters
    }
    
    /// Appends the parameterized headers to the request
    public func appendHeaders(_ append: Headers) {
        headers += append
    }
    
    /// Generates a URLRequest based on the current state of the instance and the parameterized environment
    public func urlRequest(for environment: Environment) -> URLRequest {
        // skip applying the environment if we are config'd to do so
        if !applyEnvironment { return urlRequest() }
        
        // apply the environment to the body
        body?.apply(environment)
        
        // combine the default headers from the env with the ones specified for this request
        headers += environment.defaultHeaders
        
        // generate the absolute url from the path and base
        let url = environment.url(appending: path)
        // instantiate the request
        return URLRequest(method: method, url: url, body: body, headers: headers, queryParameters: queryParameters)
    }
    
    private func urlRequest() -> URLRequest {
        // generate the absolute url from the path and base
        let url = URL(string: path)!
        // instantiate the request
        return URLRequest(method: method, url: url, body: body, headers: headers, queryParameters: queryParameters)
    }
}

/// Combines lhs and rhs into a single parameter dictionary, key conflicts are resolved using rhs
fileprivate func +(left: Headers?, right: Headers?) -> Headers? {
    // if we don't have lhs, return rhs
    guard let left = left else {
        return right
    }
    
    // if we don't have rhs, return lhs
    guard let right = right else {
        return left
    }
    
    // othwerwise if we have both, combine them resolving key conflicts by taking rhs
    return left.merging(right, uniquingKeysWith: { _, new in new })
}

fileprivate func +=(left: inout Headers?, right: Headers?) {
    left = left + right
}
