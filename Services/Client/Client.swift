//
//  Client.swift
//  Services
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

open class Client {
    /// The environment for the service layer to hit
    public let environment: Environment
    /// The object that handles authenticating requests
    public var authenticator: Authenticator?
    /// We are going to use the shared session until we need more control
    public let session: URLSession
    /// The decoder that will be used to serialize responses
    public let decoder = JSONDecoder()
    
    public init(environment: Environment, authenticator: Authenticator? = nil, configuration: URLSessionConfiguration = .default) {
        self.environment = environment
        self.authenticator = authenticator
        self.session = URLSession(configuration: configuration)
    }
    
    /// Allows the caller to provide behavior based on the response
    public typealias OnComplete<T: Codable> = (T?, Error?) -> Void
    
    /// Will make the request and attempt to serialize a model of type Codable T
    @discardableResult
    public func model<T: Codable>(request: Request, _ completion: @escaping OnComplete<T>) -> URLSessionTask {
        let request = prepare(request)
        
        let task = self.session
            .dataTask(with: request) { data, response, error in
                guard let data = data else {
                    DispatchQueue.main.async { completion(nil, error) }
                    return
                }
                
                do {
                    let model = try self.decoder.decode(T.self, from: data)
                    DispatchQueue.main.async { completion(model, error) }
                }
                catch {
                    DispatchQueue.main.async {
                        fatalError("JSON parsing failed with error \(error)")
                    }
                }
            }
            
        task.resume()
        
        return task
    }
    
    /// Will make the request and attempt to serialize an array of models of type Codable T
    @discardableResult
    public func models<T: Codable>(request: Request, _ completion: @escaping OnComplete<[T]>) -> URLSessionTask {
        let request = prepare(request)
        
        let task = self.session
            .dataTask(with: request) { data, response, error in
                guard let data = data else {
                    DispatchQueue.main.async { completion(nil, error) }
                    return
                }
                
                do {
                    let model = try self.decoder.decode([T].self, from: data)
                    DispatchQueue.main.async { completion(model, error) }
                }
                catch {
                    DispatchQueue.main.async {
                        fatalError("JSON parsing failed with error \(error)")
                    }
                }
        }
        
        task.resume()
        
        return task
    }
    
    func prepare(_ request: Request) -> URLRequest {
        // authenticate the request if we have an authenticator and the request is config'd as authenticated
        if request.authenticated {
            authenticator?.authenticate(request, environment: environment)
        }
        
        // generate the URLRequest against our env
        let request = request.urlRequest(for: environment)
        
        return request
    }
}
