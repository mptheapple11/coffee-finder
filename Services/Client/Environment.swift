//
//  Environment.swift
//  Services
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

/// Used to configure an instance of the service layer
public struct Environment {
    /// The hostname of the environment
    public let hostname: String
    /// The scheme of the environment
    public let scheme: Scheme
    /// Path common to all requests against the environment
    public let basePath: String
    
    /// Parameters that should be applied to all requests
    public let defaultParameters: Parameters?
    /// Headers that should be applied to all requests
    public let defaultHeaders: Headers?
    
    public init(hostname: String, scheme: Scheme = .https, basePath: String,
        defaultParameters: Parameters? = nil, defaultHeaders: Headers? = nil) {
        self.hostname          = hostname
        self.scheme            = scheme
        self.basePath          = basePath
        self.defaultParameters = defaultParameters
        self.defaultHeaders    = defaultHeaders
    }
    
    /// Scheme type to be used for requests
    public enum Scheme: String {
        case http
        case https
    }
    
    // MARK: - Computed
    
    public var baseURL: URL {
        var components = URLComponents()
        components.scheme = scheme.rawValue
        components.host = hostname
        
        return components.url!
    }
    
    public func url(appending path: String, queryItems: [URLQueryItem] = []) -> URL {
        var components = URLComponents()
        components.scheme = scheme.rawValue
        components.host = hostname
        components.path = (basePath + "/\(path)").applyingForwardSlashes()
        
        let items = queryItems
        components.queryItems = items.count > 0 ? items : nil
        
        return components.url!
    }
}

extension String {
    /// Safely applys forward slashes between path components (prevents "//" when appending relative path to the base path)
    func applyingForwardSlashes() -> String {
        // first split on the forward slash to get path components
        var pathComponents = components(separatedBy: "/")
        // filter out empty path components
        pathComponents = pathComponents.filter { return !$0.isEmpty }
        // re-join the path components with / and prepend a single /
        return "/\(pathComponents.joined(separator: "/"))"
    }
}
