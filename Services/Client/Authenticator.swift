//
//  Authenticator.swift
//  Services
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

/// Provides the necessary headers and/or parameters to authenticate each request
public protocol Authenticator {
    func authenticate(_ request: Request, environment: Environment)
}

