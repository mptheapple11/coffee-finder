//
//  ModelBindable.swift
//  FoundationExt
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import Foundation

/// Used to describe a type that is data driven by an optional model type
public protocol ModelBindable {
    associatedtype Model
    var model: Model? { get set }
}

/// Used to describe a type that is instantiated with a model, non-optional
public protocol ModelInstantiated {
    associatedtype Model
    var model: Model { get }
    
    init(_ model: Model)
}
