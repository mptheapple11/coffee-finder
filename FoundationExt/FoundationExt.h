//
//  FoundationExt.h
//  FoundationExt
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for FoundationExt.
FOUNDATION_EXPORT double FoundationExtVersionNumber;

//! Project version string for FoundationExt.
FOUNDATION_EXPORT const unsigned char FoundationExtVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FoundationExt/PublicHeader.h>


