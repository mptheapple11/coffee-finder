//
//  JSON.swift
//  FoundationExt
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

/// Type definition for JSON
public typealias JSON = [String: Any]

/// Facilitates the conversion to JSON
public protocol JSONEncodable {
    func encoded() -> JSON?
}

/// Adds JSONRepresentable conformance to all Encodables
public extension JSONEncodable where Self: Encodable {
    func encoded() -> JSON? {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        
        guard let data = try? encoder.encode(self),
            let encoded = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? JSON else {
                return nil
        }
        return encoded
    }
}

/// Adds JSONEncodable conformance to JSON by returning self
extension JSON: JSONEncodable {
    public func encoded() -> JSON? {
        return self
    }
}

public extension JSON {
    /// The resulting string has the form "key=value&key2=value2".
    func queryString() -> String {
        return sorted { (rhs, lhs) -> Bool in
            rhs.key < lhs.key
        }
        .reduce([], { (result, pair) -> [String] in
            // make a mutable copy of result
            var mutable = result
            // percent encode the value
            let encodedValue = "\(pair.value)".addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            // create and append the key/value string to the result
            mutable.append("\(pair.key)=\(encodedValue!)")
            
            return mutable
        })
        .joined(separator: "&")
    }
}
