//
//  UIKitExt.h
//  UIKitExt
//
//  Created by Michael Place on 5/19/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for UIKitExt.
FOUNDATION_EXPORT double UIKitExtVersionNumber;

//! Project version string for UIKitExt.
FOUNDATION_EXPORT const unsigned char UIKitExtVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <UIKitExt/PublicHeader.h>


