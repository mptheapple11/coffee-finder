//
//  Storyboard+Instantiable.swift
//  UIKitExt
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit

public protocol StoryboardInstantiable {
    static var storyboardName: String { get }
    static var identifier: String { get }
    static func instance() -> Self
}

extension UIViewController: StoryboardInstantiable {
    open class var storyboardName: String {
        // naming convention is DetailViewController -> Detail.storyboard but can be overriden
        return String(describing: self).replacingOccurrences(of: "ViewController", with: "")
    }
    
    open class var identifier: String {
        // naming convention is class name
        return String(describing: self)
    }
    
    public static func instance() -> Self {
        return genericInstance()
    }
    
    static func genericInstance<T: UIViewController>() -> T {
        // instantiate the storyboard
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle(for: self))
        // check to see if we can pull out a view controller of the correct type
        guard let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
            fatalError("View controller of type \(String(describing: self)) was not found with identifier \(identifier)")
        }
        
        return viewController
    }
}
