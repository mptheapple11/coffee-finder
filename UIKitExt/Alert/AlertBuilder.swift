//
//  AlertBuilder.swift
//  UIKitExt
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit

/// Thin wrapper around UIAlertAction initializer
public class Action {
    private let _title: String?
    private let _style: UIAlertAction.Style
    private let _handler: ActionHandler?
    private let _included: Bool
    
    /// Called when an action is selected
    public typealias ActionHandler = (UIAlertAction) -> Void
    
    /// Convenience initializer for a default action with a default title of "OK"s
    public static func ok(included: Bool = true, title: String = "OK", _ handler: ActionHandler? = nil) -> Action {
        return Action(included: included, title: title, style: .default, handler)
    }
    
    /// Convenience initializer for a default action
    public static func `default`(included: Bool = true, title: String, _ handler: ActionHandler? = nil) -> Action {
        return Action(included: included, title: title, style: .default, handler)
    }
    
    /// Convenience initializer for a cancel action with a default title of "Cancel"
    public static func cancel(included: Bool = true, title: String = "Cancel", _ handler: ActionHandler? = nil) -> Action {
        return Action(included: included, title: title, style: .cancel, handler)
    }
    
    /// Convenience initializer for a destructive action
    public static func destructive(included: Bool = true, title: String, _ handler: ActionHandler? = nil) -> Action {
        return Action(included: included, title: title, style: .destructive, handler)
    }
    
    public init(included: Bool = true, title: String? = nil, style: UIAlertAction.Style = .default, _ handler: ActionHandler? = nil) {
        self._title = title
        self._style = style
        self._handler = handler
        self._included = included
    }
    
    func build() -> UIAlertAction? {
        if !_included { return nil}
        
        return UIAlertAction(title: _title, style: _style, handler: _handler)
    }
}

/// Thin wrapper around UIAlertController initializer
public class AlertBuilder {
    let _title: String?
    let _message: String?
    let _style: UIAlertController.Style
    let _actions: [Action]
    let _tintColor: UIColor?
    
    /// Convenience initializer where all params are optional including a single action
    convenience init(title: String? = nil, message: String? = nil, style: UIAlertController.Style = .alert, tintColor: UIColor? = nil, action: Action? = nil) {
        self.init(title: title, message: message, style: style, tintColor: tintColor, actions: [action!])
    }
    
    /// Initializer where all params are optional including an array of actions
    public init(title: String? = nil, message: String? = nil, style: UIAlertController.Style = .alert, tintColor: UIColor? = nil, actions: [Action] = []) {
        self._title = title
        self._message = message
        self._style  = style
        self._actions = actions
        self._tintColor = tintColor
    }
    
    /// Presents the alert from the parameterized viewController
    public func present(from viewController: UIViewController, animated: Bool = true, _ completion: (() -> Void)? = nil) {
        let alert = build()
        viewController.present(alert, animated: animated, completion: {
            alert.view.tintColor = self._tintColor
            completion?()
        })
    }
    
    /// Builds an instance of UIAlertController based on the current state of the builder
    public func build() -> UIAlertController {
        let alert = UIAlertController(title: _title, message: _message, preferredStyle: _style)
        alert.view.tintColor = _tintColor
        _actions.compactMap { $0.build() }
            .forEach { alert.addAction($0) }
        
        return alert
    }
}
