//
//  UIApplication+URLs.swift
//  UIKitExt
//
//  Created by Michael Place on 5/20/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit

/// Its safe to bang all of these because they are system urls
public extension UIApplication {
    struct URLs {
        /// URL to open iOS settings app
        public static let openSettings = URL(string: UIApplication.openSettingsURLString)!
    }
}

