//
//  UIImageView+URL.swift
//  UIKitExt
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import UIKit

public extension UIImageView {
    func load(_ url: URL?) {
        guard let url = url else { image = nil; return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                UIView.transition(
                    with: self,
                    duration: 0.2,
                    options: .transitionCrossDissolve,
                    animations: {
                        self.image = UIImage(data: data!)
                    },
                    completion: nil
                )
            }
        }.resume()
    }
}
