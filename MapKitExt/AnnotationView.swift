//
//  AnnotationView.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import FoundationExt
import MapKit

open class AnnotationView<Model: MKAnnotation>: MKMarkerAnnotationView, ModelBindable {
    public typealias Model = Model
    
    open var model: Model? {
        didSet { annotation = model }
    }
}
