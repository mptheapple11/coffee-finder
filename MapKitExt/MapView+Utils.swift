//
//  MapView+Utils.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import MapKit

public extension MKMapView {
    /// Centers the map on the parameterized location and radius
    func centerOn(_ location: CLLocation, radius: CLLocationDistance = 1000) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: radius, longitudinalMeters: radius)
        setRegion(coordinateRegion, animated: true)
    }
    
    /// Deques an instance of the generic MKAnnotationView subclass, otherwise instantiates a new one and applies the config closure
    func dequeueReusableAnnotationView<View: MKAnnotationView, Annotation: MKAnnotation>(ofType: View.Type, for annotation: Annotation, configure: (View, Annotation) -> Void) -> View {
        let identifier = String(describing: View.self)
        
        register(View.self, forAnnotationViewWithReuseIdentifier: identifier)
        
        guard let dequeuedView = dequeueReusableAnnotationView(withIdentifier: String(describing: View.self), for: annotation) as? View else {
            let view = View.init(annotation: annotation, reuseIdentifier: identifier)
            configure(view, annotation)
            return view
        }
        
        configure(dequeuedView, annotation)
        
        return dequeuedView
    }
}
