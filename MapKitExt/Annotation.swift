//
//  Annotation.swift
//  CoffeeFinder
//
//  Created by Michael Place on 5/22/20.
//  Copyright © 2020 Fooda. All rights reserved.
//

import FoundationExt

open class Annotation<Model>: NSObject, ModelInstantiated {
    public typealias Model = Model
    public var model: Model
    
    required public init(_ model: Model) {
        self.model = model
    }
}
